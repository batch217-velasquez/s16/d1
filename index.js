console.log("Hello World!");

// [SECTION] Arithmetic Operators
// + , - , * , /, %

let x = 3;
let y = 10;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of Modulo operator: " + remainder);

// [SECTION] Assignment Operator
// Basic Assignemnt operator is (=) equal sign.

let assignementNumber = 8;

// Addition Assignment
assignementNumber = assignementNumber + 2;
console.log("Result of addition assignement operator: " +assignementNumber);

// Shorthand for assignmentNumber = assignmentNumber + 2 
assignementNumber += 2;
console.log("Result of addition assignement operator: " +assignementNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)
assignementNumber -= 2;
console.log("Result of Subtraction assignement operator: " +assignementNumber);

assignementNumber *= 2;
console.log("Result of Multiplication assignement operator: " +assignementNumber);

assignementNumber /= 2;
console.log("Result of division assignement operator: " +assignementNumber);

// Multiple Operators and Parentheses


let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

//incrementation and decrementation
 // Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//pre-incrementation
let increment = ++z;
console.log("Result of pre-incrmenet: " + increment);
console.log("Result of pre-incrmenet: " + z);

//post-incrementation
increment = z++;
console.log("Result of post-incrmenet: " + increment);

//decrementation
//pre-decremaentation
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

//post-decremaentation
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// [SECTION] Type Coercion
//Type coercion is the automatic or implicit conversion of values from one data type to another

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//true = 1
let numE = true + 1;
console.log(numE);

//false = 0
let numF = false + 1;
console.log(numF);

// [SECTION] Comparison Operators
let juan = "juan";

//Equality Operator (==)
// = assignment of value
// == Comparision Equality

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);

//IneQuality Operator (!=)
// ! = not

console.log(1 != 1);
console.log(1 != 2);

//Strict Equlity Operator (!==)
  /* 
            -Checks if the value or operand are equal are of the same type
   */

   console.log(1 === '1');
   console.log('juan' === juan);
   console.log(0 === false);

//Strict Inequality Operator

console.log(1 !== '1');
console.log('juan' !== juan);
console.log(0 !== false);

//[SECTION] Relational Operator
//Some comparison operators check whether one value is greater or less than to the other value.
// >, <, =

let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a >= b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

// [SECTION] Logical Operators
// && --> AND , || --> OR , ! --> NOT

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logial && Result: " + allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logial OR Result: " + someRequirementsMet);

let someRequirmentsNotMet = !isRegistered;
console.log("Logial NOT Result: " + someRequirmentsNotMet);


